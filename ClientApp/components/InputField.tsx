﻿import * as React from 'react';

export default class InputContainer extends React.Component<IInputContainerProps, {}> {
    render() {
        return <input type="text" onChange={(e) => this.props.onChange(e.target.value)} />
    }
}

export interface IInputContainerProps {
    onChange(text: string): void
}