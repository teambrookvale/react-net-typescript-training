﻿import * as React from 'react';
import InputField from './InputField'

export default class InputContainter extends React.Component<any, IInputContainterState> {
    constructor(props: any) {
        super(props)

        this.state = {
            currentValue: ''
        }
    }

    render() {
        const isUppercase = this.state.currentValue.length > 0 && this.state.currentValue === this.state.currentValue.toUpperCase();

        return <div>
            <InputField onChange={(val) => this.setState({ currentValue: val }) }/>
            <p>{this.state.currentValue}</p>
            {isUppercase && <p>ALL UPPERCASE</p>}
            <button
                onClick={() => {
                    fetch(`https://httpbin.org/get`)
                        .then(response => response.json() as Promise<any>)
                        .then(data => {
                            console.log(data);
                            this.setState({ currentValue: data.url })
                        });
                }}
            >Google</button>
        </div>
    }
}

interface IInputContainterState {
    currentValue: string
}