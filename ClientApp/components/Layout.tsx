import * as React from 'react';
import Home from './Home';

export class Layout extends React.Component<{}, {}> {
    public render() {
        return <div className='container'>
            <div className='row'>
                <Home />
            </div>
            <div className='row'>
                { this.props.children }
            </div>
        </div>;
    }
}
