# React .NET Typescript Tutorial
------------------

Download the following tools, open react-net-typescript-training.csproj in Visual Studio 2017 Preview and hit play:

* [.NET Core SDK](https://www.microsoft.com/net/download/thank-you/dotnet-sdk-2.0.3-windows-x64-installer)
* [nodeJS](https://nodejs.org/dist/v8.9.1/node-v8.9.1-x64.msi)
* [Visual Studio Community 2017 PREVIEW](https://www.visualstudio.com/thank-you-downloading-visual-studio/?ch=pre&sku=Community&rel=15)